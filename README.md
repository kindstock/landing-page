# KindStock Landing Page

Temporary landing page until we are ready to move to a real
shopping cart.

## Usage

### Prereqs

Docker: https://docs.docker.com/install/

### Instructions

To deploy a local version just run:

```
make server
```

## Making changes

You can make sylistic changes in *public/css/ks.css*.

You can make content changes in *public/index.html*.
