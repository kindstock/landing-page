DOCKER ?= docker
DKR_TAG ?= ks/web:latest

# Edit here - set path to you directory with config.json & fonts

FONT_DIR ?= ./public/vendor/fontello/
FONTELLO_HOST ?= http://fontello.com

build: font docker-build

docker-build:
	${DOCKER} build -t ${DKR_TAG} .

server: build
	${DOCKER} run -v ${CURDIR}/public:/usr/share/nginx/html -p 8080:80 ${DKR_TAG}

font: fontopen fontsave

fontopen:
	@if test ! `which curl` ; then \
		echo 'Install curl first.' >&2 ; \
		exit 128 ; \
		fi
	@curl --silent --show-error --fail --output .fontello \
		--form "config=@${FONT_DIR}/config.json" \
		${FONTELLO_HOST}

fontsave:
	@if test ! `which unzip` ; then \
		echo 'Install unzip first.' >&2 ; \
		exit 128 ; \
		fi
	@if test ! -e .fontello ; then \
		echo 'Run `make fontopen` first.' >&2 ; \
		exit 128 ; \
		fi
	@rm -rf .fontello.src .fontello.zip
	@curl --silent --show-error --fail --output .fontello.zip \
		${FONTELLO_HOST}/`cat .fontello`/get
	@unzip .fontello.zip -d .fontello.src
	@rm -rf ${FONT_DIR}
	@mv `find ./.fontello.src -maxdepth 1 -name 'fontello-*'` ${FONT_DIR}
	@rm -rf .fontello.src .fontello.zip

font-clean:
	@find ${FONT_DIR} -not -name config.json -and \
		-not -wholename ${FONT_DIR} \
		-exec rm -rf {} \;

clean: font-clean
